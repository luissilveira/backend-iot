package com.unit.TCC.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unit.TCC.model.Maquina;
import com.unit.TCC.repository.MaquinaRepository;

@Service
public class MaquinaService {

	@Autowired
	MaquinaRepository maquinaRepository;
	Maquina maquina = new Maquina();


	public List<Maquina> getAllmaquinas() {
		return (List<Maquina>) maquinaRepository.findAll();
	}
	
	public Maquina getById(Long id) {
		maquina = maquinaRepository.findOneById(id);
		System.out.println(maquina);
		return maquina;
	}

	public Maquina updateMaquinaById(Maquina maquina) {
		
		Maquina maquiRepo = maquinaRepository.findOneById(maquina.getId());
		maquiRepo.updateMaquina(maquina);
		maquiRepo = maquinaRepository.save(maquiRepo);
		
		return maquiRepo;
	}
	
	public boolean alterStatus(Long id) {
		
		Maquina mRepo = maquinaRepository.findOneById(id);
		
		if(mRepo.isStatus())
			mRepo.setStatus(false);
		else
			mRepo.setStatus(true);
		
		maquinaRepository.save(mRepo);
		
		return mRepo.isStatus();
		
	}
	
}
