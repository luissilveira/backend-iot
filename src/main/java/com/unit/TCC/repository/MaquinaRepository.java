package com.unit.TCC.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.unit.TCC.model.Maquina;

@Repository
public interface MaquinaRepository extends CrudRepository<Maquina, Long> {

	public Maquina findOneById(Long id);
	
}
