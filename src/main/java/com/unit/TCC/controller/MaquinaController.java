package com.unit.TCC.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.unit.TCC.model.Maquina;
import com.unit.TCC.service.MaquinaService;

@RestController
public class MaquinaController {

	@Autowired
	MaquinaService maquinaService;
	
	@RequestMapping(value = "/maquina/{id}", method= RequestMethod.GET)
	public @ResponseBody Maquina getMaquinaById(@PathVariable Long id) {
		
		return maquinaService.getById(id);
	}
	
	@RequestMapping(value = "/maquina/", method= RequestMethod.PATCH)
	public @ResponseBody Maquina updateMaquina(@RequestBody Maquina maquina) {
		
		
		return maquinaService.updateMaquinaById(maquina);
	}
	
	@RequestMapping(value = "/maquina/status/{id}", method= RequestMethod.PATCH)
	public @ResponseBody boolean alterStatus(@PathVariable Long id) {
		
		
		return maquinaService.alterStatus(id);
	}
	
}
