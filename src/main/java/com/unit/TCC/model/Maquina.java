package com.unit.TCC.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Maquina {
	
	public Maquina(Maquina maquina) {
		this.setId(maquina.id);
		this.setLitrosAguaUtilizado(maquina.litrosAguaUtilizado);
		this.setProdutosProduzidos(maquina.produtosProduzidos);
		this.setQuantidadeAcucar(maquina.quantidadeAcucar);
		this.setStatus(maquina.status);
	}
	
	public Maquina() {
		// TODO Auto-generated constructor stub
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private int produtosProduzidos;
	
	private double litrosAguaUtilizado;
	
	private double quantidadeAcucar;
	
	private boolean status;
	
	
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getProdutosProduzidos() {
		return produtosProduzidos;
	}
	public void setProdutosProduzidos(int produtosProduzidos) {
		this.produtosProduzidos = produtosProduzidos;
	}
	public double getLitrosAguaUtilizado() {
		return litrosAguaUtilizado;
	}
	public void setLitrosAguaUtilizado(double litrosAguaUtilizado) {
		this.litrosAguaUtilizado = litrosAguaUtilizado;
	}

	public double getQuantidadeAcucar() {
		return quantidadeAcucar;
	}

	public void setQuantidadeAcucar(double quantidadeAcucar) {
		this.quantidadeAcucar = quantidadeAcucar;
	}

	public void updateMaquina(Maquina maquina){
		this.setLitrosAguaUtilizado(maquina.litrosAguaUtilizado);
		this.setProdutosProduzidos(maquina.produtosProduzidos);
		this.setQuantidadeAcucar(maquina.quantidadeAcucar);
	}
	
	public boolean alteraStatus(Maquina maquina) {
		
		if(maquina.status)
			maquina.setStatus(false);
		else
			maquina.setStatus(true);
		
		return maquina.status;
		
	}
	
}
