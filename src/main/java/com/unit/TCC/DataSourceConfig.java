package com.unit.TCC;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
public class DataSourceConfig {
    
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceBuilder.url("jdbc:mysql://localhost:3306/tcc?useSSL=false&useTimezone=true&serverTimezone=America/Sao_Paulo");
        dataSourceBuilder.username("root");
        dataSourceBuilder.password("root");
        
        return dataSourceBuilder.build();
    }
    
    @Configuration
    @EnableTransactionManagement
    public class PersistenceJPAConfig{
     
       @Bean
       public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
          LocalContainerEntityManagerFactoryBean em 
            = new LocalContainerEntityManagerFactoryBean();
          em.setDataSource(getDataSource());
          em.setPackagesToScan(new String[] { "com.unit.TCC.model" });
     
          JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
          em.setJpaVendorAdapter(vendorAdapter);
          em.setJpaProperties(additionalProperties());
     
          return em;
       }
       
       @Bean
       public PlatformTransactionManager transactionManager() {
           JpaTransactionManager transactionManager = new JpaTransactionManager();
           transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        
           return transactionManager;
       }
        
       @Bean
       public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
           return new PersistenceExceptionTranslationPostProcessor();
       }
        
       Properties additionalProperties() {
           Properties properties = new Properties();
           properties.setProperty("hibernate.hbm2ddl.auto", "update");
           properties.setProperty("hibernate.show_sql", "true");
           properties.setProperty("hibernate.format_sql", "true");
           properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
              
           return properties;
       }
     
    }
}

